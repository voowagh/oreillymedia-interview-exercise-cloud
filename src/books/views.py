from django.shortcuts import render

from .models import Book
from rest_framework import generics
from .serializers import BookSerializer


class BookList(generics.ListAPIView):
    # API endpoint that allows books to be viewed.
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    filter_fields = (
        'work_id',
        'title',
        'authors',
        'isbn',
    )

class BookDetail(generics.RetrieveAPIView):
    # API endpoint that returns a single book by pk.
    queryset = Book.objects.all()
    serializer_class = BookSerializer