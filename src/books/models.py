from django.db import models


class Book(models.Model):
    '''
    Book Model

    Schema: public
    Table: works
    '''
    work_id        = models.IntegerField("work_id", db_column="work_id", primary_key=True)
    title          = models.TextField("title", db_column="title")
    authors        = models.TextField("authors", db_column="authors")
    isbn           = models.TextField("isbn", db_column="isbn")
    description    = models.TextField("description", db_column="description")

    class Meta:
        # managed = False
        db_table = 'works'

    def __str__(self):
        return self.work_id