FROM python:3-alpine as base


FROM base as builder

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /code
COPY src/requirements.txt /code/

RUN mkdir /py-install
RUN pip install --prefix=/py-install -r requirements.txt



FROM base

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

COPY --from=builder /py-install /usr/local

WORKDIR /code
COPY src/ /code/

CMD ["./wait-for", "ormdb:5432", "-t", "0", "--", "python", "manage.py", "runserver", "0.0.0.0:8000"]
