# interview-exercise-cloud

This repository contains a solution to the interview exercise found here:
```
https://github.com/oreillymedia/interview-exercise-cloud/blob/main/README.md
```

## Local Testing

A `docker-compose.yml` file is included, to facilitate rapid testing of solutions.

You can run this to test using docker-compose:
```bash
docker-compose up
```

## REST API

### Setup

No special setup is required. Once started, the container hosting the REST API will wait for the postgres service on the `ormdb` container to become available, then begin serving the REST API.

To pull the container from DockerHub:
```bash
docker pull registry.hub.docker.com/uuw0oceez3upi/orm-bais9eo-solution-1
```

### Usage

The REST API to inspect works in the DB is available at:
```
http://<host>:8000/books/
```

That base view will return all works in the DB, and the details for an individual work can be accessed by its `work_id`:
```
http://<host>:8000/books/<work_id>
```

You can also filter results to show subsets based on title, authors, and isbn:
```
http://<host>:8000/books/?title=Python%20Fundamentals
http://<host>:8000/books/?authors=Bill%20Lubanovic
http://<host>:8000/books/?isbn=9781492055020
```

## Excercise Description

The O'Reilly search API provides information on the many books which are available on our Learning Platform. Your assignment is to develop a REST API in either Python or GoLang. The API should include endpoints that allow users to get information on all of the books it has stored, a subset of these books and a single book.

Finally, containerize your application as if you were deploying it to a Kubernetes cluster.

A PostgreSQL container image with an initial data set has been provided in the instructions below.

## Database

### Setup

We have provided a PostgreSQL container with an initial set of data loaded (200 books). Use this data to create your API endpoints.

To pull this container from DockerHub:
```bash
docker pull registry.hub.docker.com/caedus41/oreilly-cloud-engineer-postgres
```

The credentials for this database are:
```bash
username = oreilly
password = hunter2
```

You can run this container using the following command:
```
docker run --name ormdb -e POSTGRES_PASSWORD=hunter2 -e POSTGRES_USER=oreilly -d registry.hub.docker.com/caedus41/oreilly-cloud-engineer-postgres
```

### Schema Info

All of the data is loaded in the `public` schema within one `works` table. 

The fields in this table are:
```
   Column    |       Type        |
-------------+-------------------+
 work_id     | integer           |
 title       | text              |
 authors     | text              |
 isbn        | character varying |
 description | text              |
 ```
